
function testConnect() {
    
    //wysy�anie danych do aktywnej karty
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { data: "hello" }, null, function (response) {
            console.log("ResponseFromActiveTab:"+ response);
        });
    });

    //Wysy�anie danych do t�a
    chrome.runtime.sendMessage({ data: "longData" }, null, function (response) {
        console.log("ResponseFromBackground:" + response);
    });
}

function readTabsAmounts(tabs) {
    var tabsCount = tabs.length;
    var regex = RegExp('^https?:\/\/');
    for (var i = 0; i < tabsCount; i++) {
        if (regex.test(tabs[i].url)) {//pomijanie dzia�a� na karcie je�li jest to karta wewn�trzna od chroma
            chrome.tabs.sendMessage(tabs[i].id, { data: tabsCount + "I'm tab with number " + i + " and id " + tabs[i].id }, null, function (response) {
                console.log("ResponseFromActiveTab:" + response);
            });
        }
    }
}

document.addEventListener('DOMContentLoaded', function () {

  document.querySelector('#testConnect').addEventListener(
        'click', testConnect);

  chrome.tabs.query({}, readTabsAmounts);
});